# v2

#### 介绍
LayuiCdnV2雏型，欢迎大家提交，希望全局模块化，用多少取多少的原则，而非全部加载预取
并且在原有API接口结构上

#### API接口
 **域名** api.layuicdn.com

 **协议** https

 **方式** 主POST 副GET

 **示例** 无需参数GET [https://api.layuicdn.com/GetLayuiVersion](https://api.layuicdn.com/GetLayuiVersion)

 **注意** 如果是需要{传入参数}的API接口，必须使用POST请求

```
1. 获取全文件版本 /GetLayuiVersion
2. 获取COS压缩包 /GetCosLayuiZipListObjects
3. 获取第三方组件-公共（参数page:1，limit:10）/GetLayuiCdnExtends
4. 获取第三方组件-用户（参数token:xxx，page:1，limit:10）/GetLayuiCdnUserExtend
5. 新建第三方组件-用户（参数token:xxx，name:xxx，version:xxx，url:xxx）/AddLayuiCdnExtend
6. 上传第三方组件-用户（参数token:xxx，extend_token:xxx，file:{文件压缩包}）/UploadLayuiCdnExtend
7. 删除第三方组件-用户（参数token:xxx，extend_token:xxx）/DelLayuiCdnExtend
8. 获取赞助捐赠列表-公共（参数page:1，limit:10）/GetLayuiCdnSponsors
9. 获取赞助捐赠商家-公共 /GetLayuiCdnSponsor
10. 发起QQ登录 可以GET/POST请求（参数rurl:{Base64格式的回调面页地址}，type:{固定值:layuicdn}，token:{实时时间戳}） /QqLogin
-成功则会返回临时token并GET跳转到回调面页地址，token=32位 有效期30分钟
-成功：GET https://www.layuicdn.com/index.html?C9D53FDB45E225D37FBC1096A533102F
-失败：GET https://www.layuicdn.com/index.html?-1
-注意：获取到临时token相当于成功登录！后续一切操作都将会进行token验证
11. 获取用户信息（参数token:xxx） /LayuiCdnUserVerify
12. 获取服务器信息  /GetLayuiCdnServersCdh
13. 获取服务器节点  /GetLayuiCdnServer
14. 获取服务器TOP /GetLayuiCdnServersTop
15. 获取问题反馈-公共（参数page:1，limit:10）/GetLayuiCdnFeedback
16. 获取问题反馈-用户（参数token:xxx，page:1，limit:10）/GetLayuiCdnUserFeedback
17. 新建问题反馈-用户（参数token:xxx，type:1，open:0，desc:内容）/AddLayuiCdnFeedback
18. 更新问题反馈-用户（参数token:xxx，type:1，open:0，desc:内容，id:xxx）/UpdateLayuiCdnFeedback

```


#### 目录结构


```
  ├─index.html //首页
  ├─api.html //API页
  ├─extend.html //组件页
  ├─servers.html //服务器检测页
  ├─feedback.html //问题反馈页
  ├─xxxxx.html //xxx页
  ├─index //面页资源目录
  │  │─index.js //面页入口-全局调用
  │  │─css //Css目录
  │  │  ├─index_1.css
  │  │  ├─index_2.css
  │  │  └─index_3.css
  │  │─js //Js目录
  │  │  ├─index_1.js
  │  │  ├─index_2.js
  │  │  └─index_3.js
  │  │─images //图片目录
  │  │  ├─index_1.png
  │  │  ├─index_2.png
  │  │  └─index_3.png
  │  │─font //字体目录
  │  │  ├─index.eot
  │  │  ├─index.svg
  │  │  ├─index.ttf
  │  │  ├─index.woff
  │  │  └─index.woff2
  │  │─html //提示目录
  │  │  ├─403.html
  │  │  ├─404.html
  │  │  └─error.html
  └─ └─layui //核心Layui目录...略
```

